class Usuario {
    constructor(name, lastname) {
        this.name = name
        this.lastname = lastname
        this.mascota = []
        this.libros = []
    }
    
    getFullName() {
        return console.log(`Nombre: ${this.name}, Apellido: ${this.lastname}`)
    }

    addMascota(animal) {
    this.mascota.push(animal)
    return console.log(`${this.mascota}`)      
    }

    countMascotas() {
        return console.log(`Pedro tiene ${this.mascota.length} mascotas`)
    }

    addBooks(name, autor) {
        this.libros.push({name: name, autor: autor})
        return console.log(`Libro añadido`)
    }
    
    getBooksNames() {
        const booksByName = []
        return (
            this.libros.forEach(el => booksByName.push(el.name)),
            // console.log(`Libros de ${this.name}: `),
            console.log(booksByName)
            )
    }
    
}

let pedro = new Usuario('Pedro', 'Roca')

pedro.getFullName()
pedro.addMascota('Lola')
pedro.addMascota('Peluchin')
pedro.countMascotas()

pedro.addBooks('Al Calor del Verano', 'John Katzenbach')
pedro.addBooks('Habitos atomicos', 'James Clear')
pedro.getBooksNames()


// let emilce = new Usuario('Emilce', 'Saenz')

// emilce.getFullName()
// emilce.addMascota('Zoe')
// emilce.addMascota('Pucky')
// emilce.countMascotas()

// emilce.addBooks('Al Calor del Verano 2', 'John Katzenbach')
// emilce.addBooks('Habitos atomicos 2', 'James Clear')
// emilce.getBooksNames()